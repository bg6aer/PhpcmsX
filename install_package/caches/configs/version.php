<?php
return array(
    'ue4_version'=> 'v2.6.8', //UE4修复版本号
    'ue4_release'=> '20200914', //UE4修复版更新日期
    'pc_version' => 'V9.6.3',	//phpcms 版本号
    'pc_release' => '20170515',	//phpcms 更新日期
);
?>